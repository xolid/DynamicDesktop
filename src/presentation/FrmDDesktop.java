/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import javax.swing.ImageIcon;
import tools.RenderList;

/**
 *
 * @author CarlosAlberto
 */
public class FrmDDesktop extends javax.swing.JFrame {

    private File file;
    private List<File> history = new ArrayList();

    /**
     * Creates new form FrmDDesktop
     */
    public FrmDDesktop() {
        initComponents();
        file = new File(new File("").getAbsolutePath());
        updateContent();
    }

    private void updateContent() {
        //actualizando url
        txtUrl.setText(file.getAbsolutePath());
        //actualizando contenido de la url
        if (file.exists()) {
            if (file.isDirectory()) {
                //objetos para almacenar el contenido
                List names = new ArrayList();
                HashMap elements = new HashMap<>();
                //objetos para organizar el contenido
                File[] files = file.listFiles();
                Arrays.sort(files, new tools.DirectoryComparator());
                //cargando el contenido
                for (File f : files) {
                    ImageIcon icon = null;
                    if (f.isDirectory()) {
                        if(chkCount1.isSelected()){
                            try{
                                String filename = "";
                                int count = f.listFiles().length;
                                if(count<20){
                                    filename = "folder-20.png";
                                }else{
                                    if(count>=20&&count<=200){
                                        filename = "folder20-200.png";
                                    }else{
                                        if(count>200){
                                            filename = "folder200+.png";
                                        }else{
                                            filename = "folder0.png";
                                        }
                                    }
                                }
                                icon = new ImageIcon(this.getClass().getResource("/resources/"+filename));
                            }catch(NullPointerException e){
                                icon = new ImageIcon(this.getClass().getResource("/resources/folder0.png"));
                            }
                        }else{
                            icon = new ImageIcon(this.getClass().getResource("/resources/folder.png"));
                        }
                    } else {
                        if(chkSize1.isSelected()){
                            String filename = "";
                            long size = f.length();
                            if(size<2097152){
                                filename = "file-2M.png";
                            }else{
                                if(size>=2097152&&size<=209715200){
                                    filename = "file2-200M.png";
                                }else{
                                    if(size>209715200L&&size<=2147483648L){
                                        filename = "file2-2G.png";
                                    }else{
                                        if(size>2147483648L){
                                            filename = "file2G+.png";
                                        }else{
                                            filename = "file0.png";
                                        }
                                    }
                                }
                            }
                            icon = new ImageIcon(this.getClass().getResource("/resources/"+filename));
                        }else{
                            icon = new ImageIcon(this.getClass().getResource("/resources/file.png"));
                        }
                    }
                    names.add(f.getName());
                    elements.put(f.getName(), icon);
                }
                lstContent.setCellRenderer(new RenderList(elements));
                lstContent.setListData(names.toArray());
            }
            txtUrl.setBackground(Color.GREEN);
        } else {
            txtUrl.setBackground(Color.RED);
        }
        updateBackButton();
    }

    private void updateBackButton() {
        if (history.isEmpty()) {
            btnBack.setEnabled(false);
        } else {
            btnBack.setEnabled(true);
        }
    }

    private void addToHistory(File f) {
        if (history.size() > 0) {
            if (!history.get(history.size() - 1).equals(f)) {
                history.add(f);
            }
        } else {
            history.add(f);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnGo = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        btnUp = new javax.swing.JButton();
        btnHome = new javax.swing.JButton();
        txtUrl = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstContent = new javax.swing.JList();
        jMenuBar1 = new javax.swing.JMenuBar();
        mnuMostrar = new javax.swing.JMenu();
        chkSize1 = new javax.swing.JCheckBoxMenuItem();
        chkCount1 = new javax.swing.JCheckBoxMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Dynamic Desktop");
        setIconImage(new ImageIcon(getClass().getResource("/resources/home&food.png")).getImage());

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Comandos de Navegacion:"));

        btnGo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/goto.png"))); // NOI18N
        btnGo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoActionPerformed(evt);
            }
        });

        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/1leftarrow.png"))); // NOI18N
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnUp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/1downarrow.png"))); // NOI18N
        btnUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpActionPerformed(evt);
            }
        });

        btnHome.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/home&food.png"))); // NOI18N
        btnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHomeActionPerformed(evt);
            }
        });

        txtUrl.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtUrlKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnHome, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnUp, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtUrl, javax.swing.GroupLayout.DEFAULT_SIZE, 520, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnHome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnGo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnBack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnUp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtUrl))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Contenido del folder:"));
        jPanel2.setToolTipText("");

        lstContent.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstContent.setLayoutOrientation(javax.swing.JList.HORIZONTAL_WRAP);
        lstContent.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstContentMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(lstContent);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                .addContainerGap())
        );

        mnuMostrar.setText("Mostrar");

        chkSize1.setText("Tamaño de Archivo");
        chkSize1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkSize1ItemStateChanged(evt);
            }
        });
        mnuMostrar.add(chkSize1);

        chkCount1.setText("Archivos Contenidos");
        chkCount1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkCount1ItemStateChanged(evt);
            }
        });
        mnuMostrar.add(chkCount1);

        jMenuBar1.add(mnuMostrar);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnGoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoActionPerformed
        // TODO add your handling code here:
        if (!txtUrl.getText().equals("")) {
            addToHistory(file);
            file = new File(txtUrl.getText());
            updateContent();
        }
    }//GEN-LAST:event_btnGoActionPerformed

    private void txtUrlKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUrlKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER) {
            addToHistory(file);
            file = new File(txtUrl.getText());
            updateContent();
        }
    }//GEN-LAST:event_txtUrlKeyReleased

    private void btnUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpActionPerformed
        // TODO add your handling code here:
        addToHistory(file);
        file = new File(txtUrl.getText().substring(0, txtUrl.getText().lastIndexOf(java.io.File.separator) + 1));
        updateContent();
    }//GEN-LAST:event_btnUpActionPerformed

    private void btnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHomeActionPerformed
        // TODO add your handling code here:
        addToHistory(file);
        file = new File(new File("").getAbsolutePath());
        updateContent();
    }//GEN-LAST:event_btnHomeActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        if (history.size() > 0) {
            file = history.remove(history.size() - 1);
            updateContent();
        }
    }//GEN-LAST:event_btnBackActionPerformed

    private void lstContentMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstContentMouseClicked
        // TODO add your handling code here:
        if (evt.getClickCount() > 1) {
            File tmp = new File(txtUrl.getText() + java.io.File.separator + lstContent.getSelectedValue());
            if (tmp.exists()) {
                if (tmp.isDirectory()) {
                    addToHistory(file);
                    file = tmp;
                    updateContent();
                }
            }
        }
    }//GEN-LAST:event_lstContentMouseClicked

    private void chkSize1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkSize1ItemStateChanged
        // TODO add your handling code here:
        updateContent();
    }//GEN-LAST:event_chkSize1ItemStateChanged

    private void chkCount1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkCount1ItemStateChanged
        // TODO add your handling code here:
        updateContent();
    }//GEN-LAST:event_chkCount1ItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmDDesktop.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmDDesktop.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmDDesktop.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmDDesktop.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmDDesktop().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnGo;
    private javax.swing.JButton btnHome;
    private javax.swing.JButton btnUp;
    private javax.swing.JCheckBoxMenuItem chkCount1;
    private javax.swing.JCheckBoxMenuItem chkSize1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList lstContent;
    private javax.swing.JMenu mnuMostrar;
    private javax.swing.JTextField txtUrl;
    // End of variables declaration//GEN-END:variables
}
