/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dynamicdesktop;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import presentation.FrmDDesktop;

/**
 *
 * @author CarlosAlberto
 */
public class DynamicDesktop {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here      
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch ( ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
        }
        FrmDDesktop frmDDesktop = new FrmDDesktop();
        frmDDesktop.setVisible(true);
    }
    
}
