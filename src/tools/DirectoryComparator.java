/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import java.io.File;
import java.util.Comparator;

/**
 *
 * @author CarlosAlberto
 */
public class DirectoryComparator implements Comparator {

    @Override   
    public int compare(Object a, Object b) {
        File filea = (File) a;
        File fileb = (File) b;
        if (filea.isDirectory() && !fileb.isDirectory()) {
            return -1;
        } else {
            if (!filea.isDirectory() && fileb.isDirectory()) {
                return 1;
            } else {
                return filea.getName().compareToIgnoreCase(fileb.getName());
            }
        }
    }

}
