/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import java.awt.Component;
import java.util.HashMap;
import javax.swing.*;

public class RenderList extends JLabel implements ListCellRenderer {

    HashMap<Object, ImageIcon> elements;
    ImageIcon nullicon = new ImageIcon(this.getClass().getResource("/resources/file.png"));

    public RenderList(HashMap<Object, ImageIcon> elements) {
        this.elements = elements;
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        if (elements.get(value) != null) {
            setIcon(elements.get(value));
            setText("" + value);
        } else {
            setIcon(nullicon);
            setText("" + value);
        }
        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        setEnabled(list.isEnabled());
        setFont(list.getFont());
        setOpaque(true);
        return this;
    }

}
